import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class SnakeGame {

    private static final int ROWS = 5;
    private static final int COLS = 10;

    private static final char WALL = '1';
    private static final char APPLE = '8';
    private static final char HEAD = '6';
    private static final char BODY = '3';
    private static final char TAIL = '5';
    private static final char EMPTY = '.';

    private static char[][] grid = new char[ROWS][COLS];
    private static int snakeLength = 3;
    private static int[] snakeX = new int[ROWS * COLS];
    private static int[] snakeY = new int[ROWS * COLS];
    private static int appleX;
    private static int appleY;
    private static int directionX = 1;
    private static int directionY = 0;

    public static void main(String[] args) {
        initializeGrid();
        spawnApple();

        KeyListener keyListener = new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {
            }

            @Override
            public void keyPressed(KeyEvent e) {
                int keyCode = e.getKeyCode();
                if (keyCode == KeyEvent.VK_UP) {
                    directionX = 0;
                    directionY = -1;
                } else if (keyCode == KeyEvent.VK_DOWN) {
                    directionX = 0;
                    directionY = 1;
                } else if (keyCode == KeyEvent.VK_LEFT) {
                    directionX = -1;
                    directionY = 0;
                } else if (keyCode == KeyEvent.VK_RIGHT) {
                    directionX = 1;
                    directionY = 0;
                }
            }

            @Override
            public void keyReleased(KeyEvent e) {
            }
        };

        Frame frame = new Frame();
        frame.addKeyListener(keyListener);
        frame.setSize(400, 200);
        frame.setVisible(true);

        while (true) {
            updateSnake();
            drawGrid();
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private static void initializeGrid() {
        for (int i = 0; i < ROWS; i++) {
            for (int j = 0; j < COLS; j++) {
                if (i == 0 || i == ROWS - 1 || j == 0 || j == COLS - 1) {
                    grid[i][j] = WALL;
                } else {
                    grid[i][j] = EMPTY;
                }
            }
        }

        // Set initial snake position
        int initialX = COLS / 2;
        int initialY = ROWS / 2;
        grid[initialY][initialX] = HEAD;
        snakeX[0] = initialX;
        snakeY[0] = initialY;
        for (int i = 1; i < snakeLength - 1; i++) {
            grid[initialY][initialX - i] = BODY;
            snakeX[i] = initialX - i;
            snakeY[i] = initialY;
        }
        grid[initialY][initialX - snakeLength + 1] = TAIL;
        snakeX[snakeLength - 1] = initialX - snakeLength + 1;
        snakeY[snakeLength - 1] = initialY;
    }

    private static void spawnApple() {
        while (true) {
            int x = (int) (Math.random() * (COLS - 2)) + 1;
            int y = (int) (Math.random() * (ROWS - 2)) + 1;
            if (grid[y][x] == EMPTY) {
                grid[y][x] = APPLE;
                appleX = x;
                appleY = y;
                break;
            }
        }
    }

    private static void updateSnake() {
        int newX = snakeX[0] + directionX;
        int newY = snakeY[0] + directionY;

        if (grid[newY][newX] == WALL || grid[newY][newX] == BODY || grid[newY][newX] == TAIL) {
            System.out.println("Game Over!");
            System.exit(0);
        }

        // Move the tail
        int prevX = snakeX[snakeLength - 1];
        int prevY = snakeY[snakeLength - 1];
        grid[prevY][prevX] = EMPTY;

        // Move the body
        for (int i = snakeLength - 1; i > 0; i--) {
            snakeX[i] = snakeX[i - 1];
            snakeY[i] = snakeY[i - 1];
            grid[snakeY[i]][snakeX[i]] = BODY;
        }

        // Move the head
        snakeX[0] = newX;
        snakeY[0] = newY;
        grid[newY][newX] = HEAD;

        if (newX == appleX && newY == appleY) {
            // Snake ate the apple
            snakeLength++;
            grid[prevY][prevX] = BODY;
            spawnApple();
        }
    }

    private static void drawGrid() {
        clearConsole();
        for (int i = 0; i < ROWS; i++) {
            for (int j = 0; j < COLS; j++) {
                System.out.print(grid[i][j]);
            }
            System.out.println();
        }
    }

    private static void clearConsole() {
        try {
            if (System.getProperty("os.name").contains("Windows")) {
                new ProcessBuilder("cmd", "/c", "cls").inheritIO().start().waitFor();
            } else {
                System.out.print("\033[H\033[2J");
                System.out.flush();
            }
        } catch (Exception e) {
            // Do nothing
        }
    }
}
