import java.util.Scanner;
import java.io.IOException;
import java.util.Random;

public class SnakeGameTerminal {

    private static final int ROWS = 30;
    private static final int COLS = 60;

    private static final char WALL = '1';
    private static final char APPLE = '8';
    private static final char HEAD = '6';
    private static final char BODY = '3';
    private static final char TAIL = '5';
    private static final char EMPTY = ' ';

    private static char[][] grid = new char[ROWS][COLS];
    private static int snakeLength = 3;
    private static int[] snakeX = new int[ROWS * COLS];
    private static int[] snakeY = new int[ROWS * COLS];
    private static int appleX;
    private static int appleY;
    private static int directionX = 1;
    private static int directionY = 0;
    private static boolean modeShadow = false;
    private static Random r=new Random();

    public static void main(String[] args) {
        

        System.out.println("Welcome to Snake!");
        System.out.println("Use arrow keys to move.");
        //afficheMenu();

        initializeGrid();
        spawnApple();
        try {
            while (true) {
                if (System.in.available() > 0) {
                    char inputChar = (char) System.in.read();
                    handleInput(inputChar);
                }

                updateSnake();
                drawGrid();
                Thread.sleep(400);
            }
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }
    private static void afficheMenu(){
        String [] mot=new String[]{"Jouer","Difficultée","Mode Shadow","Quitter"};
    }

    
    private static void handleInput(char input) {
        if (input == 'z') {
            // Up arrow key
            directionX = 0;
            directionY = -1;
        } else if (input == 's') {
            // Down arrow key
            directionX = 0;
            directionY = 1;
        } else if (input == 'd') {
            // Right arrow key
            directionX = 1;
            directionY = 0;
        } else if (input == 'q') {
            // Left arrow key
            directionX = -1;
            directionY = 0;
        }
    }

    private static void initializeGrid() {
        for (int i = 0; i < ROWS; i++) {
            for (int j = 0; j < COLS; j++) {
                if (i == 0 || i == ROWS - 1 || j == 0 || j == COLS - 1) {
                    grid[i][j] = WALL;
                } else {
                    grid[i][j] = EMPTY;
                }
            }
        }
        creerObstacles(5);// 1 => 20
        // Set initial snake position
        
        int initialX = 3;
        int initialY = 1;
        grid[initialY][initialX] = HEAD;
        snakeX[0] = initialX;
        snakeY[0] = initialY;
        for (int i = 1; i < snakeLength - 1; i++) {
            grid[initialY][initialX - i] = BODY;
            snakeX[i] = initialX - i;
            snakeY[i] = initialY;
        }
        grid[initialY][initialX - snakeLength + 1] = TAIL;
        snakeX[snakeLength - 1] = initialX - snakeLength + 1;
        snakeY[snakeLength - 1] = initialY;
    }
    private static void creerObstacles(int difficultee) {
		for (int i=2; i < ROWS-2; i++) {
		    for (int j=2; j < COLS-2; j++) {
            if ( difficultee <= 5   && r.nextInt(30) < difficultee
                                    && grid[i-1][j-1] != WALL
                                    && grid[i-1][j+1] != WALL
                                    && grid[i+1][j-1] != WALL
                                    && grid[i+1][j+1] != WALL
                                    && grid[i][j+1] != WALL
                                    && grid[i][j-1] != WALL
                                    && grid[i+1][j] != WALL
                                    && grid[i-1][j] != WALL ) {
			    grid[i][j] = WALL;
			}else if (difficultee > 5   && r.nextInt(30) < difficultee
                                        && grid[i-1][j-1] != WALL
			                            && grid[i-1][j+1] != WALL
			                            && grid[i+1][j-1] != WALL
			                            && grid[i+1][j+1] != WALL ) {
			    grid[i][j] = WALL;
			}


		    }
		}
    }

    private static void spawnApple() {
        while (true) {
            int x = (int) (Math.random() * (COLS - 2)) + 1;
            int y = (int) (Math.random() * (ROWS - 2)) + 1;
            if (grid[y][x] == EMPTY) {
                grid[y][x] = APPLE;
                appleX = x;
                appleY = y;
                break;
            }
        }
    }

    private static void updateSnake() {
        int newX = snakeX[0] + directionX;
        int newY = snakeY[0] + directionY;

        if (grid[newY][newX] == WALL || grid[newY][newX] == BODY || grid[newY][newX] == TAIL) {
            System.out.println("Game Over!");
            System.exit(0);
        }

        // Move the tail
        int prevX = snakeX[snakeLength - 1];
        int prevY = snakeY[snakeLength - 1];
        grid[prevY][prevX] = EMPTY;

        // Move the body
        for (int i = snakeLength - 1; i > 0; i--) {
            snakeX[i] = snakeX[i - 1];
            snakeY[i] = snakeY[i - 1];
            grid[snakeY[i]][snakeX[i]] = BODY;
        }

        // Move the head
        snakeX[0] = newX;
        snakeY[0] = newY;
        grid[newY][newX] = HEAD;

        if (newX == appleX && newY == appleY) {
            // Snake ate the apple
            snakeLength++;
            grid[prevY][prevX] = BODY;
            spawnApple();
        }
    }

    private static void drawGrid() {
        clearConsole();
        for (int i = 0; i < ROWS; i++) {
            for (int j = 0; j < COLS; j++) {
                if(modeShadow){
                    char c=grid[i][j];
                    if( c == WALL || c == BODY || c == HEAD || c == Tail ){
                        System.out.print(c);
                    }else {
                        //imposible
                    }
                }else{
                    System.out.print(grid[i][j]);
                }
                if((i == 0 || i == ROWS-1 ) && 0 != j && j != COLS-1){
                    System.out.print("1");
                }else if( 0 < j && j < COLS-1 ){
                    System.out.print(".");
                }
            }
            System.out.println();
        }
    }

    private static void clearConsole() {
        try {
            if (System.getProperty("os.name").contains("Windows")) {
                new ProcessBuilder("cmd", "/c", "cls").inheritIO().start().waitFor();
            } else {
                System.out.print("\033[H\033[2J");
                System.out.flush();
            }
        } catch (Exception e) {
            // Do nothing
        }
    }
}
