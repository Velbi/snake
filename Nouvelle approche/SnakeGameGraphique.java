import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class SnakeGameGraphique extends JFrame implements KeyListener {
    private static final int ROWS = 30;
    private static final int COLS = 30;
    private static final char EMPTY = '.';
    private static final char WALL = '1';
    private static final char APPLE = '8';
    private static final char HEAD = '6';
    private static final char BODY = '3';
    private static final char TAIL = '5';

    private char[][] grid;
    private int snakeLength;
    private int[] snakeRow;
    private int[] snakeCol;
    private int appleRow;
    private int appleCol;
    private Direction direction;

    private enum Direction {
        UP, DOWN, LEFT, RIGHT
    }

    public SnakeGameGraphique() {
        grid = new char[ROWS][COLS];
        snakeRow = new int[ROWS * COLS];
        snakeCol = new int[ROWS * COLS];
        snakeLength = 3;
        direction = Direction.RIGHT;

        initializeGrid();
        placeSnake();
        placeApple();

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setTitle("Snake Game");
        setSize(400, 200);
        setLocationRelativeTo(null);
        addKeyListener(this);
        setFocusable(true);
        setFocusTraversalKeysEnabled(false);
        setVisible(true);

        gameLoop();
    }

    private void initializeGrid() {
        for (int i = 0; i < ROWS; i++) {
            for (int j = 0; j < COLS; j++) {
                if (i == 0 || i == ROWS - 1 || j == 0 || j == COLS - 1) {
                    grid[i][j] = WALL;
                } else {
                    grid[i][j] = EMPTY;
                }
            }
        }
    }

    private void placeSnake() {
        int initialRow = ROWS / 2;
        int initialCol = COLS / 2;
        grid[initialRow][initialCol] = HEAD;
        snakeRow[0] = initialRow;
        snakeCol[0] = initialCol;
        for (int i = 1; i < snakeLength; i++) {
            grid[initialRow][initialCol - i] = BODY;
            snakeRow[i] = initialRow;
            snakeCol[i] = initialCol - i;
        }
    }

    private void placeApple() {
        int row, col;
        do {
            row = (int) (Math.random() * (ROWS - 2)) + 1;
            col = (int) (Math.random() * (COLS - 2)) + 1;
        } while (grid[row][col] != EMPTY);

        grid[row][col] = APPLE;
        appleRow = row;
        appleCol = col;
    }

    private void gameLoop() {
        while (true) {
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            move();
            if (isCollision()) {
                System.out.println("Game Over!");
                break;
            }
            if (grid[snakeRow[0]][snakeCol[0]] == APPLE) {
                snakeLength++;
                placeApple();
            }
            repaint();
        }
    }

    private void move() {
        int newHeadRow = snakeRow[0];
        int newHeadCol = snakeCol[0];

        switch (direction) {
            case UP:
                newHeadRow--;
                break;
            case DOWN:
                newHeadRow++;
                break;
            case LEFT:
                newHeadCol--;
                break;
            case RIGHT:
                newHeadCol++;
                break;
        }

        if (grid[newHeadRow][newHeadCol] == WALL || grid[newHeadRow][newHeadCol] == BODY) {
            return;
        }

        if (grid[newHeadRow][newHeadCol] == APPLE) {
            snakeLength++;
            placeApple();
        } else {
            int tailRow = snakeRow[snakeLength - 1];
            int tailCol = snakeCol[snakeLength - 1];
            grid[tailRow][tailCol] = EMPTY;
        }

        for (int i = snakeLength - 1; i > 0; i--) {
            snakeRow[i] = snakeRow[i - 1];
            snakeCol[i] = snakeCol[i - 1];
        }

        snakeRow[0] = newHeadRow;
        snakeCol[0] = newHeadCol;

        grid[newHeadRow][newHeadCol] = HEAD;
        grid[snakeRow[1]][snakeCol[1]] = BODY;
        grid[snakeRow[snakeLength - 1]][snakeCol[snakeLength - 1]] = TAIL;
    }

    private boolean isCollision() {
        int headRow = snakeRow[0];
        int headCol = snakeCol[0];
        return grid[headRow][headCol] == WALL || grid[headRow][headCol] == BODY;
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);

        int cellSize = 20;

        for (int i = 0; i < ROWS; i++) {
            for (int j = 0; j < COLS; j++) {
                char cell = grid[i][j];
                if (cell == WALL) {
                    g.setColor(Color.BLACK);
                } else if (cell == HEAD) {
                    g.setColor(Color.RED);
                } else if (cell == BODY) {
                    g.setColor(Color.GREEN);
                } else if (cell == TAIL) {
                    g.setColor(Color.BLUE);
                } else if (cell == APPLE) {
                    g.setColor(Color.ORANGE);
                } else {
                    g.setColor(Color.WHITE);
                }
                g.fillRect(j * cellSize, i * cellSize, cellSize, cellSize);
            }
        }
    }

    @Override
    public void keyPressed(KeyEvent e) {
        int key = e.getKeyCode();
        if (key == KeyEvent.VK_UP && direction != Direction.DOWN) {
            direction = Direction.UP;
        } else if (key == KeyEvent.VK_DOWN && direction != Direction.UP) {
            direction = Direction.DOWN;
        } else if (key == KeyEvent.VK_LEFT && direction != Direction.RIGHT) {
            direction = Direction.LEFT;
        } else if (key == KeyEvent.VK_RIGHT && direction != Direction.LEFT) {
            direction = Direction.RIGHT;
        }
    }

    @Override
    public void keyTyped(KeyEvent e) {}

    @Override
    public void keyReleased(KeyEvent e) {}

    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> new SnakeGameGraphique());
    }
}
