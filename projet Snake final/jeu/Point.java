package jeu;
class Point {
		int lT;
		int cT;  
		int lQ; 
		int cQ;
		public Point(int lT, int cT, int lQ, int cQ) {
			
			this.lT = lT;
			this.cT = cT;
			this.lQ = lQ;
			this.cQ = cQ;
		}
		public int getlT() {
			return lT;
		}
		public void setlT(int lT) {
			this.lT = lT;
		}
		public int getcT() {
			return cT;
		}
		public void setcT(int cT) {
			this.cT = cT;
		}
		public int getlQ() {
			return lQ;
		}
		public void setlQ(int lQ) {
			this.lQ = lQ;
		}
		public int getcQ() {
			return cQ;
		}
		public void setcQ(int cQ) {
			this.cQ = cQ;
		}
		
}
