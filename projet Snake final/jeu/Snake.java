import extensions.*;

class Snake extends Program {
	char deplacement = 'd'; 
    char deplacement2 = 'd';
    boolean fin = false;
    boolean modeGraphique=false;
    int difficulte=1;
    int joueur=1;
    int place=0;
    boolean activation_cuseur=true;
    boolean valide=false;

    public static void main(String[] args) {
    	
    	Snake s=new Snake();
	s.menu();	
	s.activation_cuseur=false;
	int hauteur=35;
	int largeur=35;
	Image img = newImage(largeur*20,hauteur*20);
	setGrid(img,largeur,hauteur);
	s.clear();
	
	enableKeyTypedInConsole(false);
	if(!s.fin){
	    if (s.joueur==1) {
		int compteur = s.questions();
		   
		boolean collision = false;
		int[][] monde = s.initialisation(hauteur,largeur);
		char [][]mouvement = new char[length(monde,1)][length(monde,2)];
		coordonne coord = new coordonne(); 
		monde = creerObstacles(monde,compteur);
		monde = creerSnake(monde,5,1);
		coord = initialisation_mouvement(monde,coord,mouvement,4,2); 
		boolean pomme = false;
		pomme=implantation_pomme(pomme,monde);
		clear();
		if (modeGraphique) {
		    show(img);
		} else {
		    enableKeyTypedInConsole(true);
		}
		while (!fin) {
		    if (modeGraphique) {
			def_couleur(monde,img);
		    } else {
			println(afficher(monde,difficulte));
		    }
		    delay(400);
		    suprimer_queue(monde,mouvement,coord);	    
		    mouvement[coord.lT][coord.cT]=deplacement;
		    coord=changement_coord_et_mouvement(pomme,mouvement,coord,monde);
		    collision=rencontre_obstacle(monde,coord,collision);
		    pomme=implantation_pomme(pomme,monde);
		    pomme=rencontre_pomme(pomme,monde,coord);
		    modification(monde,coord,4,2);
		    clear();
		}
		if (collision) {
		    println("Vous vous �tes �cras� !");
		} else {
		    println("fin du jeu");
		}
	    } else {
		int[][] monde = initialisation(hauteur,largeur);
		println("Tour du Joueur 1");
		int compteur = questions();
		println("Tour du Joueur 2");
		int compteur2 = questions();
		boolean collision=false;
		boolean collision2=false;
		monde = creerObstacles(monde,8);
		monde = creerSnake(monde,compteur,1);
		monde = creerSnake(monde,compteur2,2);
		char [][]mouvement = new char[length(monde,1)][length(monde,2)];
		char [][]mouvement2 = new char[length(monde,1)][length(monde,2)];
		coordonne coord = new coordonne();
		coordonne coord2 = new coordonne();
		coord = initialisation_mouvement(monde,coord,mouvement,4,2);
		coord2 = initialisation_mouvement(monde,coord2,mouvement2,6,5);
		boolean pomme = false;
		boolean pomme2 = false;
		pomme = implantation_pomme(pomme,monde);
		pomme2 = implantation_pomme(pomme2,monde);
		clear();
		if (modeGraphique) {
		    show(img);
		} else {
		    enableKeyTypedInConsole(true);
		}
		while (!fin) {
		    if (modeGraphique) {
			def_couleur(monde,img);
		    } else {
			println(afficher(monde,difficulte));
		    }
		    delay(400);
		    suprimer_queue(monde,mouvement,coord);
		    suprimer_queue(monde,mouvement2,coord2);
		    mouvement[coord.lT][coord.cT]=deplacement;
		    mouvement2[coord2.lT][coord2.cT]=deplacement2;
		    coord=changement_coord_et_mouvement(pomme,mouvement,coord,monde);
		    coord2=changement_coord_et_mouvement(pomme2,mouvement2,coord2,monde);
		    pomme=implantation_pomme(pomme,monde);
		    pomme2=implantation_pomme(pomme2,monde);
		    pomme=rencontre_pomme(pomme,monde,coord);
		    pomme2=rencontre_pomme(pomme2,monde,coord2);
		    collision=rencontre_obstacle(monde,coord,collision);
		    collision2=rencontre_obstacle(monde,coord2,collision2);
		    modification(monde,coord,4,2);
		    modification(monde,coord2,6,5);
		    clear();
		}
		victoire_defaite(collision,collision2);
	    }
	}
    }

    void clear() {
	clearScreen();
	cursor(0,0);    
    }
    
    void menu(){	
    	boolean fin_du_menu=false;
    	activation_cuseur=true;
    	String [] mot=new String[]{"Jouer","Description","Param�tre","Quitter"};
    	String [] parametre=new String[]{"Mode graphique","Joueur","Difficulte","Revenir menu"};
    	place=0;
    	while(!fin_du_menu){
	    
	    enableKeyTypedInConsole(true);
	    clear();
	    String []tab=new String[4];
	    for (int i=0;i<length(tab) ;i++ ) {
		tab=mot;
    		
    		
		cursor(i+1, 0);
		if (i==place) {
		    print(ANSI_BLUE+i+". "+tab[i]+ANSI_WHITE );     				
		}else{
		    print(i+". "+tab[i]);
		}
		if (i==0) {
		    print("                Nombre de joueur: "+joueur);
		}else if (i==1) {
		    print("          Commencer au niveaux: "+difficulte);
		}
    		
    		
    		
	    }
	    cursor(6, 0);
	    println("Pour valider : fl�che de droite");  
	    delay(50);
	    if (place>3 && place!=99) {
		place=0;
	    }
	    if (place<0 && place!=99) {
		place=3;	
	    }  		  		
	    if (valide) {
		if(place==0){
		    fin_du_menu=true;
		}else if (place==1) {
		    valide=false;
		    while(!valide){
			print("\u001b[2J");
			cursor(0, 0);
			println("Ce jeu regoupe une s�rie de questions de calcul mental puis un snake d'une certaine difficult� en fonction de vos r�ponses \n \rappuyer sur valider pour revenir");
			delay(50);
		    }
		}else if(place==2){
		    place=0;
		    valide=false;   				
		    do{	
			
			clear();
	    		
			tab=parametre;
			for (int j=0;j<length(tab) ;j++ ) {
			    cursor(j+1, 0);
			    if (j==place) {
				print(ANSI_BLUE+j+". "+tab[j]+ANSI_WHITE );
			    }else{
				print(j+". "+tab[j]);
			    }
			    if (j==0) {
				if (modeGraphique) {
				    print(ANSI_GREEN + " activ�e");
				}else{
				    print(ANSI_RED + " d�sactiv�e");
				}	
			    }else if (j==1) {
				print(ANSI_PURPLE +" "+ joueur);
			    }else if (j==2) {
				print(ANSI_PURPLE +" "+ difficulte);
			    }
			    print(ANSI_WHITE);							
			}
			cursor(6, 0);
			println("Pour valider : fl�che de droite");  
			delay(100);
			if (place>3 && place!=99) {
			    place=0;
			}
			if (place<0 && place!=99) {
			    place=3;	
			} 			    				
			if (valide) {
			    if(place==0){
				modeGraphique=!modeGraphique;
			    }else if(place==1){
				joueur=joueur+1;
				if (joueur==3) {
				    joueur=1;				
				}    							    						
			    }else if(place==2){
				difficulte=difficulte+1;
				if (difficulte==3) {
				    difficulte=1;					
				}
			    }else if(place==3){
				place=99;
			    }
			    valide=false;
			}
		    }while(place!=99);
		    place=0;
		}else if(place==3){
		    
		    fin=true;
		    
		    fin_du_menu=true;
		    
		}
		valide=false;
	    }
    	}
    	activation_cuseur=false;
    }
    
    void victoire_defaite(boolean collision, boolean collision2) {
	if (collision && collision2) {
	    print("Joueur 1 et joueur 2 ont perdu !");
	    println("");
	} else if (collision) {
	    print("Joueur 1 a perdu !");
	    println("");
	} else if (collision2) {
	    print("Joueur 2 a perdu !");
	    println("");
	}
    }
   
    void keyTypedInConsole(char key) {
	switch(key) {
	case ANSI_UP:			    
	    if (activation_cuseur) {
		place--;
	    }else{
		deplacement = 'z';
	    }
	    break;
	case ANSI_DOWN:			    
	    if (activation_cuseur) {
		place++;
	    }else{
		deplacement = 's';
	    }
	    break;
	case ANSI_LEFT:
	    deplacement = 'q';
	    break;
	case ANSI_RIGHT:
	    if (activation_cuseur) {
		valide=true;
	    }else{
		deplacement = 'd';
	    }
	    break;
	case 'z':
	    deplacement2 = 'z';
	    break;
	case 's':
	    deplacement2 = 's';
	    break;
	case 'q':
	    deplacement2 = 'q';
	    break;
	case 'd':
	    deplacement2 = 'd';
	    break;
			
	}
    }

    void keyChanged(char key, String event) {
	switch(key) {
	case ANSI_UP:
	    deplacement = 'z';
	    break;
	case ANSI_DOWN:
	    deplacement = 's';
	    break;
	case ANSI_LEFT:
	    deplacement = 'q';
	    break;
	case ANSI_RIGHT:
	    deplacement = 'd';
	    break;
	case 'z':
	    deplacement2 = 'z';
	    break;
	case 's':
	    deplacement2 = 's';
	    break;
	case 'q':
	    deplacement2 = 'q';
	    break;
	case 'd':
	    deplacement2 = 'd';
	    break;
	}
    }
    
    int questions() {
	int compteur = 0;

	for (int i=1; i<11; i++) {
	    int chiffre1 = (int)(random()*11);
	    int chiffre2 = (int)(random()*11);
	    println("Question num�ro " + i + " sur 10 :");
	    println("Combien font " + chiffre1 + " * " + chiffre2 + " ?");		    
	    boolean recommence=false;
	    String reponse_s;
	    do{	
		recommence=false;
		reponse_s=readString();
		for (int j=0;j<length(reponse_s) ;j++ ) {
		    int converti=(int)(charAt(reponse_s,j))-(int)('0');
		    if (!(converti<=9 && converti>=0)) {
			recommence=true;
		    }
		}
		if (length(reponse_s)>3) {
		    recommence=true;
		}
	    }while(recommence);
	    int reponse=stringToInt(reponse_s);
	    if (reponse == (chiffre1*chiffre2)) {
		compteur = compteur + 1;
	    }
	    clear();
	}
	println("Vous avez " + compteur + " bonnes r�ponses sur 10.");
	return compteur;
    }
    
    int[][] initialisation(int hauteur, int largeur) {
	int[][] monde = new int[hauteur][largeur];
	for (int ligne=0; ligne<length(monde,1); ligne++) {
	    for (int colonne=0; colonne<length(monde,2); colonne++) {
		if (ligne==0 || colonne==0 || ligne==length(monde,1)-1 || colonne==length(monde,2)-1) {
		    monde[ligne][colonne]=1;
		} else {
		    monde[ligne][colonne]=0;
		}
	    }
	}
	return monde;
    }

    void testInitialistation() {
	assertArrayEquals(new int[][] {{1,1,1,1},
				       {1,0,0,1},
				       {1,0,0,1},
				       {1,1,1,1}}, initialisation(4,4));
        assertArrayEquals(new int[][]{{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
				      {1,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
				      {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}}, initialisation(3,15));
    }

    int[][] creerObstacles(int[][] mondePlein,int compteur) {
	double pourcentage=0.0;
	if (compteur == 1 || compteur == 2 || compteur == 0) {
	    pourcentage = 8.0;
	} else if (compteur == 3 || compteur == 4) {
	    pourcentage = 2.5;
	} else if (compteur == 5 || compteur == 6) {
	    pourcentage = 2.0;
	} else if (compteur == 7 || compteur == 8) {
	    pourcentage = 1.5;	    
	} else if (compteur == 9 || compteur == 10) {
	    pourcentage = 1.0;
	}
	for (int i=2; i<length(mondePlein,1)-2; i++) {
	    for (int j=2; j<length(mondePlein,2)-2; j++) {
		if (random()*11 < pourcentage && mondePlein[i-1][j-1] != 1
		    && mondePlein[i-1][j+1] != 1
		    && mondePlein[i+1][j-1] != 1
		    && mondePlein[i+1][j+1] != 1) {
		    mondePlein[i][j] = 1;
		}
	    }
	}
	return mondePlein;
    }

    void testCreerObstacles() {
	int[][] mondeTest = new int[][]{{1,1,1,1,1,1,1,1},
					{1,0,0,0,0,0,0,1},
					{1,0,0,0,0,0,0,1},
					{1,0,0,0,0,0,0,1},
					{1,0,0,0,0,0,0,1},
					{1,1,1,1,1,1,1,1}};
	mondeTest=creerObstacles(mondeTest,(int)(random()*11));
	boolean zeroOUun = false;
	
	for (int idxL=0; idxL<length(mondeTest,1);idxL++) {
	    for (int idxC=0; idxC<length(mondeTest,2);idxC++) {
		if (mondeTest[idxL][idxC]==1 || mondeTest[idxL][idxC]==0) {
		    zeroOUun = true;
		} else {
		    zeroOUun = false;
		}
	    }
	}
	assertTrue(zeroOUun);
    }
										      

    void def_couleur(int[][] monde, Image img) {
	for (int i=0; i<length(monde,1); i++) {
	    for (int j=0; j<length(monde,2); j++) {
		
		if (difficulte==1) {
		    if (monde[i][j]==0) {
		        set(img,i,j,RGBColor.BLACK);
		    } else {
			if (monde[i][j]==1) {
			    set(img,i,j,RGBColor.WHITE);
			} else if (monde[i][j]==8) {
			    set(img,i,j,RGBColor.RED);
			} else if (monde[i][j]==4) {
			    set(img,i,j,RGBColor.BLUE);
			} else if (monde[i][j]==6) {
			    set(img,i,j,RGBColor.GREEN);
			} else {
			    set(img,i,j,RGBColor.DARKGRAY);	   
			}
		    }
		}
		
		if (difficulte==2) {
		    if (i==0 || j==0 || i==length(monde,1)-1 || j==length(monde,2)-1) {
			set(img,i,j,RGBColor.WHITE);
		    } else if (monde[i][j]>=2){
			if (monde[i][j]==8) {
			    set(img,i,j,RGBColor.RED);
			} else if (monde[i][j]==4) {
			    set(img,i,j,RGBColor.BLUE);
			} else if (monde[i][j]==6) {
			    set(img,i,j,RGBColor.GREEN);
			} else if (monde[i][j]==3 || monde[i][j]==2 || monde[i][j]==5) {
			    set(img,i,j,RGBColor.DARKGRAY);
			}
		    } else {
			if (monde[i][j-1]>=2 && monde[i][j-1]!=8  || 
			    monde[i][j+1]>=2 && monde[i][j+1]!=8  || 
			    monde[i-1][j]>=2 && monde[i-1][j]!=8  || 
			    monde[i+1][j]>=2 && monde[i+1][j]!=8  ||
			    monde[i-1][j-1]>=2 && monde[i-1][j-1]!=8 || 
			    monde[i-1][j+1]>=2 && monde[i-1][j+1]!=8 || 
			    monde[i+1][j+1]>=2 && monde[i+1][j+1]!=8 || 
			    monde[i+1][j-1]>=2 && monde[i+1][j-1]!=8 ){
			    if (monde[i][j]==0) {
				set(img,i,j,RGBColor.BLACK);
			    }else{
			        set(img,i,j,RGBColor.WHITE);
			    }					 			
			}else if (i>1 && i<length(monde,1)-2 && j<length(monde,2)-2 && j>1) {
			    if(monde[i][j-2]>=2   && monde[i][j-2]!=8 || 
			       monde[i][j+2]>=2   && monde[i][j+2]!=8 || 
			       monde[i-2][j]>=2   && monde[i-2][j]!=8 || 
			       monde[i+2][j]>=2   && monde[i+2][j]!=8 ||
			       monde[i-2][j-2]>=2 && monde[i-2][j-2]!=8 || 
			       monde[i-2][j+2]>=2 && monde[i-2][j+2]!=8 || 
			       monde[i+2][j+2]>=2 && monde[i+2][j+2]!=8 || 
			       monde[i+2][j-2]>=2 && monde[i+2][j-2]!=8 ||
			       monde[i-2][j+1]>=2 && monde[i-2][j+1]!=8 || 
			       monde[i-1][j+2]>=2 && monde[i-1][j+2]!=8 || 
			       monde[i+1][j+2]>=2 && monde[i+1][j+2]!=8 || 
			       monde[i+2][j+1]>=2 && monde[i+2][j+1]!=8 ||
			       monde[i+2][j-1]>=2 && monde[i+2][j-1]!=8 || 
			       monde[i+1][j-2]>=2 && monde[i+1][j-2]!=8 || 
			       monde[i-1][j-2]>=2 && monde[i-1][j-2]!=8 || 
			       monde[i-2][j-1]>=2 && monde[i-2][j-1]!=8 ){
				if (monde[i][j]==0) {
				    set(img,i,j,RGBColor.BLACK);	
				}else{
				    set(img,i,j,RGBColor.WHITE);
				}
				
			    }else{
				set(img,i,j,RGBColor.BLACK);
			    }
			    
			}
						
		    }
		    		
		}
	    }
	}
    }
	
    int[][] creerSnake(int[][] monde, int taille, int position) {
	int x,y,tete,queue;
	x = length(monde,2)/2;
	if (position==2) {  
	    y = length(monde,1)-2;
	    tete=6;
	    queue=5;
	} else {
	    tete=4;
	    queue=2;
	    y = 1;
	}
	if (taille < 3) {
	    taille=3;
	}
	for (int i = 0 ; i < taille ; i++) {
	    if (i==0) {
		monde[y][x] = tete;
	    } else if (i==taille-1) {
		monde[y][x-i] = queue;
	    } else {
		monde[y][x-i] = 3;
	    }
	}
	return monde;
    }

    void testCreerSnake() {
	int[][] mondeTest = new int[][]{{1,1,1,1,1,1,1,1,1,1},
					{1,0,0,0,0,0,0,0,0,1},
					{1,0,0,0,0,0,0,0,0,1},
					{1,0,0,0,0,0,0,0,0,1},
					{1,1,1,1,1,1,1,1,1,1}};
	boolean posSnake = false;
	int taille = 3;
	mondeTest=creerSnake(mondeTest,taille,1);
	if (mondeTest[1][length(mondeTest,2)/2]==4 &&
	    mondeTest[1][length(mondeTest,2)/2-(taille-1)]==2 &&
	    mondeTest[1][length(mondeTest,2)/2-1]==3) {
	    posSnake = true;
	}
	assertTrue(posSnake);
    }
	
    
    coordonne initialisation_mouvement(int [][]monde,coordonne coord,char [][]mouvement,int tete,int queue){
	for (int ligne=0;ligne<length(monde,1) ;ligne++ ) {
	    for (int colone=0;colone<length(monde,2) ;colone++ ) {
		if(monde[ligne][colone]==tete || monde[ligne][colone]==3 || monde[ligne][colone]==queue){	
		    mouvement[ligne][colone]='d';
		}
		if (monde[ligne][colone]==tete) {
		    coord.lT = ligne;
		    coord.cT = colone; 
		}else if(monde[ligne][colone]==queue){
		    coord.lQ = ligne; 
		    coord.cQ = colone; 
		}
	    }		
	}
	return coord;
    }
    
    void suprimer_queue(int [][]monde,char [][]mouvement,coordonne coord){		
	monde[coord.lQ][coord.cQ]=0;		
	monde[coord.lT][coord.cT]=3;	
    }

    coordonne changement_coord_et_mouvement(boolean pomme,char [][]mouvement,coordonne coord,int [][]monde){
	
	if (mouvement[coord.lT][coord.cT] == 'q') {
	    coord.cT = coord.cT-1;
	}
	if (mouvement[coord.lT][coord.cT]=='d') {
	    coord.cT = coord.cT+1;
	}
	if (mouvement[coord.lT][coord.cT]=='z') {
	    coord.lT=coord.lT-1;
	}
	if (mouvement[coord.lT][coord.cT]=='s') {
	    coord.lT=coord.lT+1;
	}
	if (pomme) {						
	    if (mouvement[coord.lQ][coord.cQ]=='q') {
		mouvement[coord.lQ][coord.cQ]=' ';
		coord.cQ=coord.cQ-1;		
	    }else if (mouvement[coord.lQ][coord.cQ]=='d') {
		mouvement[coord.lQ][coord.cQ]=' ';
		coord.cQ=coord.cQ+1;
	    }else if (mouvement[coord.lQ][coord.cQ]=='z') {
		mouvement[coord.lQ][coord.cQ]=' ';
		coord.lQ=coord.lQ-1;
	    }else if (mouvement[coord.lQ][coord.cQ]=='s') {
		mouvement[coord.lQ][coord.cQ]=' ';
		coord.lQ=coord.lQ+1;
	    }
	}
	return coord;
    }
    
    boolean rencontre_obstacle(int [][]monde, coordonne coord,boolean collision){
	if(monde[coord.lT][coord.cT] != 0 && monde[coord.lT][coord.cT] != 8) {
	    fin=true;
	    collision=true;
	}
	return collision;
    }

    void testRencontreObstacle() {
	int[][] mondeTest = new int[][]{{1,1,1,1,1,1,1,1},
					{1,0,1,0,0,0,0,1},
					{1,2,3,4,8,1,0,1},
					{1,0,0,1,0,1,0,1},
					{1,1,1,1,1,1,1,1}};
	boolean collision = false;
	coordonne coordTest = new coordonne();
	coordTest.cT=4;
	coordTest.lT=2;
	assertFalse(rencontre_obstacle(mondeTest,coordTest,collision));
	coordTest.cT=5;
	coordTest.lT=3;
	assertTrue(rencontre_obstacle(mondeTest,coordTest,collision));
	coordTest.cT=1;
	coordTest.lT=2;
	assertTrue(rencontre_obstacle(mondeTest,coordTest,collision));
    }
    
    boolean implantation_pomme(boolean pomme,int [][]monde){
	if(!pomme){
	    int x=0;
	    int y=0;
	    do{
		x=(int)(random()*length(monde,1));
		y=(int)(random()*length(monde,2));
	    } while (monde[x][y]!=0);
	    monde[x][y]=8;
	    pomme=true;
	}
	return pomme;
    }

    void testImplantationPomme() {
	int[][] mondeTest = new int[][]{{1,1,1,1,1},
					{1,0,1,0,1},
					{1,0,0,0,1},
					{1,0,1,0,1},
					{1,0,0,0,1},
					{1,1,1,1,1}};
	boolean unePomme=false;
	int cptPomme=0;
	implantation_pomme(false,mondeTest);
	for (int idxL=0;idxL<length(mondeTest,1);idxL++) {
	    for (int idxC=0;idxC<length(mondeTest,2);idxC++) {
		if (mondeTest[idxL][idxC]==8) {
		    cptPomme=cptPomme+1;
		}
	    }
	}
	if (cptPomme==1) {
	    unePomme=true;
	}
	assertTrue(unePomme);
    }
    
    boolean rencontre_pomme(boolean pomme, int [][]monde, coordonne coord){
	if (monde[coord.lT][coord.cT]==8) {			
	    pomme=false;
	}		
	return pomme;
    }

    void testRencontrePomme() {
	int[][] mondeTest = new int[][]{{1,1,1,1},
					{1,0,0,1},
					{1,0,8,1},
					{1,1,1,1}};
	boolean pommeTest = true;
	coordonne coordTest = new coordonne();
	coordTest.lT=2;
	coordTest.cT=2;
	assertFalse(rencontre_pomme(pommeTest,mondeTest,coordTest));
	coordTest.lT=1;
	coordTest.cT=1;
	assertTrue(rencontre_pomme(pommeTest,mondeTest,coordTest));
    }	

	
    void modification(int [][]monde,coordonne coord,int tete,int queue){
	monde[coord.lT][coord.cT]=tete;
	monde[coord.lQ][coord.cQ]=queue;
    }

    String afficher(int[][] monde, int difficulte) {
	String affichage = "";
	for (int i=0; i<length(monde,1); i++) {
	    if (i!=0) {
		affichage = affichage +"\n"+"\r";
	    }
	    for (int j=0; j<length(monde,2); j++) {
		
		if (difficulte==1) {
		    if (j!=0) {
			affichage=affichage + "'";
		    }
		    if (monde[i][j]==0) {
			affichage = affichage + " ";
		    } else {
			affichage = affichage + monde[i][j];
		    }
		}
		
		if (difficulte==2) {
		    if (j!=0) {
			affichage=affichage + "'";
		    }
		    if (i==0 || j==0 || i==length(monde,1)-1 || j==length(monde,2)-1) {
			affichage = affichage + monde[i][j];
		    } else if (monde[i][j]>=2){
			affichage = affichage + monde[i][j];
		    } else {
			if (monde[i][j-1]>=2 && monde[i][j-1]!=8  || 
			    monde[i][j+1]>=2 && monde[i][j+1]!=8  || 
			    monde[i-1][j]>=2 && monde[i-1][j]!=8  || 
			    monde[i+1][j]>=2 && monde[i+1][j]!=8  ||
			    monde[i-1][j-1]>=2 && monde[i-1][j-1]!=8 || 
			    monde[i-1][j+1]>=2 && monde[i-1][j+1]!=8 || 
			    monde[i+1][j+1]>=2 && monde[i+1][j+1]!=8 || 
			    monde[i+1][j-1]>=2 && monde[i+1][j-1]!=8 ){
			    if (monde[i][j]==0) {
				affichage = affichage + " ";	
			    }else{
				affichage = affichage + "1";
			    }					 			
			}else if (i>1 && i<length(monde,1)-2 && j<length(monde,2)-2 && j>1) {
			    if(monde[i][j-2]>=2   && monde[i][j-2]!=8 || 
			       monde[i][j+2]>=2   && monde[i][j+2]!=8 || 
			       monde[i-2][j]>=2   && monde[i-2][j]!=8 || 
			       monde[i+2][j]>=2   && monde[i+2][j]!=8 ||
			       monde[i-2][j-2]>=2 && monde[i-2][j-2]!=8 || 
			       monde[i-2][j+2]>=2 && monde[i-2][j+2]!=8 || 
			       monde[i+2][j+2]>=2 && monde[i+2][j+2]!=8 || 
			       monde[i+2][j-2]>=2 && monde[i+2][j-2]!=8 ||
			       monde[i-2][j+1]>=2 && monde[i-2][j+1]!=8 || 
			       monde[i-1][j+2]>=2 && monde[i-1][j+2]!=8 || 
			       monde[i+1][j+2]>=2 && monde[i+1][j+2]!=8 || 
			       monde[i+2][j+1]>=2 && monde[i+2][j+1]!=8 ||
			       monde[i+2][j-1]>=2 && monde[i+2][j-1]!=8 || 
			       monde[i+1][j-2]>=2 && monde[i+1][j-2]!=8 || 
			       monde[i-1][j-2]>=2 && monde[i-1][j-2]!=8 || 
			       monde[i-2][j-1]>=2 && monde[i-2][j-1]!=8 ){
				if (monde[i][j]==0) {
				    affichage = affichage + " ";	
				}else{
				    affichage = affichage + "1";
				}
				
			    }else{
				affichage = affichage + "'";
			    }
			}else{
			    affichage = affichage + "'";
			}
						
		    }
		    		
		}
	    }
	}
	affichage = affichage + "\n" + "\r";
	return affichage;
    }
   
}
