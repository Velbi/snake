import java.util.Scanner;
class projet_T_A extends Program {

	/*void keyTypedInConsole(char c) {

		if(c=='q'){
			stop();
		}
	}*/
	
	void algorithm(){
		//enableKeyTypedInConsole(true);
		int [][]plateau=new int [50][150];
		char [][]mouvement=new char [50][150];
		/*boolean debut=true;								//pour tout cacher
		int pourcentage=1;								//pourcentage de case a enlevé entre 0 et 1
		initialisation_depart(plateau,debut);
		//affichage
		int reponse = neuf_questions(plateau);	
		println(reponse);*/	
		int longeur=3;
		boolean win=false;
		coordonne co = new coordonne(); 
	    co.ligne_tete = 20;
	    co.colonne_tete = 100; 
	    co.ligne_queue = 22; 
	    co.colonne_queue = 100;  
	    
	   	plateau=initialisation_plateau(plateau,co);
	   	mouvement=initialisation_mouvement(mouvement,co);
		while(!win){
			affichage_snake(plateau);
			//affichage_char(mouvement);
			mouvement=prend_un_mouvement(mouvement,co);
			co=changement_class_mouvement(mouvement,co);
			println("co.ligne_tete="+co.ligne_tete+"\n"+
					"co.colonne_tete="+co.colonne_tete+"\n"+
					"co.ligne_queue="+co.ligne_queue+"\n"+
					"co.colonne_queue="+co.colonne_queue+"\n");
			plateau=modification(plateau,mouvement,longeur,co);

			plateau=avancement_auto(plateau,mouvement,longeur,co);
			mouvement=movement_a_suprimer(mouvement,co);
			plateau=suprimer_queue(plateau,mouvement,co);
			
		}
	}
	void affichage_question(int [][]tableau, int []nombre){
		int compteur=0;
		for (int ligne=0;ligne<length(tableau,1) ;ligne++ ) {
			for (int colone=0;colone<length(tableau,2) ;colone++ ) {
				print(tableau[ligne][colone]);
				if(length(tableau,2)<=colone+1 && ligne== 0){
					print(" vie: variable					debut");
				}
				if(length(tableau,2)<=colone+1 && ligne== length(tableau,1)*0.5){
					for (int i=0;i<length(nombre) ;i++ ) {
						if(i==1){
							print("   "+(char)nombre[i]);
						}else{
							print("   "+nombre[i]);
						}
					}
				}
				if(length(tableau,2)<=colone+1 && ligne== length(tableau,1)-1){
					print(" Score: variable					fin");
				}

			}
			print("\n");
		}
	}
	int[][] initialisation_depart(int [][] tableau, boolean debut){
		for (int ligne=0;ligne<length(tableau,1) ;ligne++ ) {
			for (int colone=0;colone<length(tableau,2) ;colone++ ) {
				if(ligne==0 || ligne==length(tableau,1)-1 || colone==0 || colone==length(tableau,2)-1){
					tableau[ligne][colone]=1;
				}else {
					tableau[ligne][colone]=5;
				}
			}
		}
		return tableau;	
	}
	int neuf_questions(int [][]tableau){
		int repondu=0;
		for (int nb_questions=0;nb_questions<=9 ;nb_questions++ ) {
		int chiffre1=(int)(random()*11);
		int chiffre2=(int)(random()*11);
		int operateur=(int)'X';
		int []operation=new int []{chiffre1,operateur,chiffre2};
		affichage_question(tableau,operation);
		println("Combien font " + chiffre1 +" X " + chiffre2 + "?");
		int reponse=readInt();

			if (reponse==(chiffre2*chiffre1)) {
				repondu++;
			}
		}
		return repondu;	
	}	
	void affichage_snake(int [][]tableau){
		for (int ligne=0;ligne<length(tableau,1) ;ligne++ ) {
			for (int colone=0;colone<length(tableau,2) ;colone++ ) {
				if(tableau[ligne][colone]==4){
					print("4");
				}else	if(ligne==0 || ligne==length(tableau,1)-1 || colone==0 || colone==length(tableau,2)-1){
					print("1");
				}else	if(ligne==(length(tableau,1)*0.5) && colone==(length(tableau,2)-1)*0.5){
					print("0");
				}else{
					print(".");
				}
			}
			print("\n");
		}
	}
	void affichage_char(char [][]tableau){
		for (int ligne=0;ligne<length(tableau,1) ;ligne++ ) {
			for (int colone=0;colone<length(tableau,2) ;colone++ ) {
				if(tableau[ligne][colone]=='z' || tableau[ligne][colone]=='q' || tableau[ligne][colone]=='s' || tableau[ligne][colone]=='d'){	
					print(tableau[ligne][colone]);
				}else{
				print(" ");
				}
			}
			print("\n");
		}
	}
	int [][]initialisation_plateau(int [][]tableau,coordonne class_coord){
		tableau[class_coord.ligne_tete][class_coord.colonne_tete]=4;
		tableau[class_coord.ligne_tete+1][class_coord.colonne_tete]=4;
		tableau[class_coord.ligne_queue][class_coord.colonne_queue]=4;
		return tableau;
	}
	char [][]initialisation_mouvement(char [][]tableau,coordonne class_coord){
		
		tableau[class_coord.ligne_tete][class_coord.colonne_tete]='z';
		tableau[class_coord.ligne_tete+1][class_coord.colonne_tete]='z';
		tableau[class_coord.ligne_tete+2][class_coord.colonne_tete]='z';
		return tableau;
	}
	char [][]prend_un_mouvement(char [][]tab,coordonne class_coord){
		//do{		
		Scanner sc = new Scanner(System.in);
		println("Veuillez saisir un deplacement :");
		String deplacement = sc.nextLine();
		//}while(equals(deplacement,'q') || equals(deplacement,'s') || equals(deplacement,'d') || equals(deplacement,'z'));
		tab[class_coord.ligne_tete][class_coord.colonne_tete]=charAt(deplacement,0);
		return tab;	
	}

	coordonne changement_class_mouvement(char [][]mouvement,coordonne class_coord){
		if (mouvement[class_coord.ligne_tete][class_coord.colonne_tete]=='q') {
			class_coord.colonne_tete=class_coord.colonne_tete-1;
			println("rooo");
		}
		if (mouvement[class_coord.ligne_tete][class_coord.colonne_tete]=='d') {
			class_coord.colonne_tete=class_coord.colonne_tete+1;
		}
		if (mouvement[class_coord.ligne_tete][class_coord.colonne_tete]=='z') {
			class_coord.ligne_tete=class_coord.ligne_tete-1;
		}
		if (mouvement[class_coord.ligne_tete][class_coord.colonne_tete]=='s') {
			class_coord.ligne_tete=class_coord.ligne_tete+1;
		}
		if (mouvement[class_coord.ligne_queue][class_coord.colonne_queue]=='q') {
			class_coord.colonne_queue=class_coord.colonne_queue-1;
		}
		if (mouvement[class_coord.ligne_queue][class_coord.colonne_queue]=='d') {
			class_coord.colonne_queue=class_coord.colonne_queue+1;
		}
		if (mouvement[class_coord.ligne_queue][class_coord.colonne_queue]=='z') {
			class_coord.ligne_queue=class_coord.ligne_queue-1;
			println("hello");
		}
		if (mouvement[class_coord.ligne_queue][class_coord.colonne_queue]=='s') {
			class_coord.ligne_queue=class_coord.ligne_queue+1;
		}
		return class_coord;
	}

	int [][] modification(int [][]plateau,char [][]mouvement,int longeur,coordonne class_coord){
			plateau[class_coord.ligne_tete][class_coord.colonne_tete]=4;
			plateau[class_coord.ligne_queue][class_coord.colonne_queue]=0;
		return plateau;
	}

	int [][]avancement_auto(int [][]plateau,char [][]mouvement,int longeur,coordonne class_coord){
		return plateau;
	}
	char [][]movement_a_suprimer(char [][]mouvement,coordonne class_coord){
		//mouvement[class_coord.ligne_queue][class_coord.colonne_queue]=' ';
		return mouvement;
	}
	int [][]suprimer_queue(int [][]plateau,char [][]mouvement,coordonne class_coord){
		
		return plateau;

	}
	//obstacle
	//pomme
	//niveaux
}
