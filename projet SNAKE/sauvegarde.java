import java.util.Scanner;
class Snake extends Program {

    void algorithm() {
		int compteur = 10;//questions();
		int[][] monde = initialisation(25,30);
		char [][]mouvement=new char[length(monde,1)][length(monde,2)];
		boolean fin = false;
		int longeur=3;
		coordonne co = new coordonne(); 
	    monde = creerObstacles(monde,compteur);
		monde = creerSnake(monde,8);
	   	co=initialisation_mouvement(monde,co,mouvement); 
		while (!fin) {
			println("co.ligne_tete="+co.ligne_tete+"\n"+
					"co.colonne_tete="+co.colonne_tete+"\n"+
					"co.ligne_queue="+co.ligne_queue+"\n"+
					"co.colonne_queue="+co.colonne_queue+"\n");
		    println(afficher(monde));
		    affichage_char(mouvement);
		    
		    co=changement_class_et_mouvement(mouvement,co,monde);
		    modification(monde,mouvement,longeur,co);
		    monde=avancement_auto(monde,mouvement,longeur,co);
		    suprimer_queue(monde,mouvement,co);
		    fin = false;
		}
	}
    
    int questions() {
		int compteur = 0;
		for (int i=1; i<11; i++) {
		    int chiffre1 = (int)(random()*11);
		    int chiffre2 = (int)(random()*11);
		    println("Question numéro " + i + " sur 10 :");
		    println("Combien font " + chiffre1 + " * " + chiffre2 + " ?");
		    int reponse = readInt();
		    if (reponse == (chiffre1*chiffre2)) {
			compteur = compteur + 1;
		    }
		}
		println("Vous avez " + compteur + " bonnes réponses sur 10.");
		return compteur;
    }

    int[][] initialisation(int hauteur, int largeur) {
		int[][] monde = new int[hauteur][largeur];
		for (int ligne=0; ligne<length(monde,1); ligne++) {
		    for (int colonne=0; colonne<length(monde,2); colonne++) {
			if (ligne==0 || colonne==0 || ligne==length(monde,1)-1 || colonne==length(monde,2)-1) {
			    monde[ligne][colonne]=1;
			} else {
			    monde[ligne][colonne]=0;
			}
		    }
		}
		return monde;
    }

    void testInitialisation() {
		int[][] testMonde = new int[][]{{0,0,0,0},
						{0,0,0,0},
						{0,0,0,0},
						{0,0,0,0}};
		assertArrayEquals(testMonde, initialisation(4,4));
		
		int[][] testMonde2 = new int[][]{{0,0,0,0,0,0,0,0,0,0,0,0},
						 {0,0,0,0,0,0,0,0,0,0,0,0}};
		assertArrayEquals(testMonde2, initialisation(2,12));
    }

    int[][] creerObstacles(int[][] mondePlein, int compteur) {
		double pourcentage=0.0;
		if (compteur == 1 || compteur == 2 || compteur == 0) {
		    pourcentage = 8.0;
		} else if (compteur == 3 || compteur == 4) {
		    pourcentage = 2.5;
		} else if (compteur == 5 || compteur == 6) {
		    pourcentage = 2.0;
		} else if (compteur == 7 || compteur == 8) {
		    pourcentage = 1.5;	    
		} else if (compteur == 9 || compteur == 10) {
		    pourcentage = 1.0;
		}
		for (int i=2; i<length(mondePlein,1)-2; i++) {
		    for (int j=2; j<length(mondePlein,2)-2; j++) {
			if (random()*11 < pourcentage && mondePlein[i-1][j-1] != 1
			                              && mondePlein[i-1][j+1] != 1
			                              && mondePlein[i+1][j-1] != 1
			                              && mondePlein[i+1][j+1] != 1) {
			    mondePlein[i][j] = 1;
			}
		    }
		}
		return mondePlein;
    }

    String afficher(int[][] monde) {
		String affichage = "";
	        for (int i=0; i<length(monde,1); i++) {
		    if (i!=0) {
			affichage = affichage + "|"+"\n";
		    }
		    for (int j=0; j<length(monde,2); j++) {
			if (monde[i][j]==0) {
			    affichage = affichage + "|" + ".";
			} else {
			    affichage = affichage + "|" + monde[i][j];
			}
		    }
		}
		affichage = affichage + "|";
		return affichage;
    }

    int[][] creerSnake(int[][] monde, int taille) {
		int middleC = length(monde,2)/2;
		for (int i = 0 ; i < taille ; i++) {
		    if (i==0) {
			monde[1][middleC] = 4;
		    } else if (i==taille-1) {
			monde[1][middleC-i] = 2;
		    } else {
			monde[1][middleC-i] = 3;
		    }
		}
		return monde;
    }

    void affichage_char(char [][]tableau){
		for (int ligne=0;ligne<length(tableau,1) ;ligne++ ) {
			for (int colone=0;colone<length(tableau,2) ;colone++ ) {
				if(tableau[ligne][colone]=='z' || tableau[ligne][colone]=='q' || tableau[ligne][colone]=='s' || tableau[ligne][colone]=='d'){	
					print(tableau[ligne][colone]);
				}else{
				print(" ");
				}
			}
			print("\n");
		}
	}

	coordonne initialisation_mouvement(int [][]monde,coordonne class_coord,char [][]mouvement){
		for (int ligne=0;ligne<length(monde,1) ;ligne++ ) {
			for (int colone=0;colone<length(monde,2) ;colone++ ) {
				if(monde[ligne][colone]==4 || monde[ligne][colone]==3 || monde[ligne][colone]==2){	
					mouvement[ligne][colone]='d';
				}
				if (monde[ligne][colone]==4) {
					class_coord.ligne_tete = ligne;
	   				class_coord.colonne_tete = colone; 
				}else if(monde[ligne][colone]==2){
					class_coord.ligne_queue = ligne; 
	    			class_coord.colonne_queue = colone; 
				}
			}		
		}
		return class_coord;
	}

	int [][]suprimer_queue(int [][]monde,char [][]mouvement,coordonne class_coord){
		monde[class_coord.ligne_queue][class_coord.colonne_queue]=0;
		

		return monde;
	}

	coordonne changement_class_et_mouvement(char [][]mouvement,coordonne class_coord,int [][]monde){
		//do{		
		Scanner sc = new Scanner(System.in);
		println("Veuillez saisir un deplacement :");
		String deplacement = sc.nextLine();
		//}while(equals(deplacement,'q') || equals(deplacement,'s') || equals(deplacement,'d') || equals(deplacement,'z'));
		mouvement[class_coord.ligne_tete][class_coord.colonne_tete]=charAt(deplacement,0);

		if (mouvement[class_coord.ligne_tete][class_coord.colonne_tete]=='q') {
			class_coord.colonne_tete=class_coord.colonne_tete-1;
				}
		if (mouvement[class_coord.ligne_tete][class_coord.colonne_tete]=='d') {
			class_coord.colonne_tete=class_coord.colonne_tete+1;
				}
		if (mouvement[class_coord.ligne_tete][class_coord.colonne_tete]=='z') {
			class_coord.ligne_tete=class_coord.ligne_tete-1;
				}
		if (mouvement[class_coord.ligne_tete][class_coord.colonne_tete]=='s') {
			class_coord.ligne_tete=class_coord.ligne_tete+1;
				}
		if (mouvement[class_coord.ligne_queue][class_coord.colonne_queue]=='q') {
			class_coord.colonne_queue=class_coord.colonne_queue-1;
			
		}
		if (mouvement[class_coord.ligne_queue][class_coord.colonne_queue]=='d') {
			class_coord.colonne_queue=class_coord.colonne_queue+1;
			
		}
		if (mouvement[class_coord.ligne_queue][class_coord.colonne_queue]=='z') {
			class_coord.ligne_queue=class_coord.ligne_queue-1;
			
		}
		if (mouvement[class_coord.ligne_queue][class_coord.colonne_queue]=='s') {
			class_coord.ligne_queue=class_coord.ligne_queue+1;
			
		}

		return class_coord;
	}

	void modification(int [][]monde,char [][]mouvement,int longeur,coordonne class_coord){
		monde[class_coord.ligne_tete][class_coord.colonne_tete]=4;
		monde[class_coord.ligne_queue][class_coord.colonne_queue]=2;
		
	}
	int [][]avancement_auto(int [][]plateau,char [][]mouvement,int longeur,coordonne class_coord){
		return plateau;
	}
	
}
