class Snake extends Program {

    char deplacement = 'd';
    boolean fin = false;
    boolean valide=false;
    void algorithm() {
    	//faire menu jouer description quitter
    	//augmentation du niveau tout les n pomme
    	//question dur ?
    	//remplacer les pommes ?
    	//faire affichage graphique
    	//proteger les entree
    	//faire une attente entre le jeux et les questions
    	class_menu menu = new class_menu();
	    menu=initialisation_menu(menu);
    	while(!menu.fin_du_jeu){
	    	println("\u001b[2J");
	    	cursor(0, 0);

	    	menu=afficher_menu(menu);
			int compteur=0;
			if (menu.questions) {
				compteur = pose_questions();
			}
			if (!menu.fin_du_snake) {				
				int[][] monde = initialisation(25,30);
				char [][]mouvement=new char[length(monde,1)][length(monde,2)];
				if (!menu.questions && menu.obstacle) {				
					do{
	    				println("Combien de réponse bonne");
						compteur=readInt();
    				}while(!(compteur>=0 && compteur<=10));	    			
				}
				if (menu.obstacle) {
					monde = creerObstacles(monde,compteur);	
				}
				
				monde = creerSnake(monde,8);
			   	coordonne coord = new coordonne(); 
			   	coord=initialisation_mouvement(monde,coord,mouvement);
			   	boolean pomme=false;
			   	pomme=inplantation_pomme(pomme,monde);

				
				
				enableKeyTypedInConsole(true);
				while (!menu.fin_du_snake) {
				    println("\u001b[2J");
		    		cursor(0, 0);
				    print(afficher(monde,menu));
				    delay(400);
				    suprimer_queue(monde,mouvement,coord);	    
				    mouvement[coord.lT][coord.cT]=deplacement;
				    coord=changement_coord_et_mouvement(pomme,mouvement,coord,monde);
				    menu=rencontre_obstacle(menu,monde,coord);
				    pomme=inplantation_pomme(pomme,monde);
				    pomme=rencontre_pomme(pomme,monde,coord);
				    modification(monde,mouvement,coord);
				    
				}
				enableKeyTypedInConsole(false);
			}
		}
		println("fin du jeu");
    }

    class_menu initialisation_menu(class_menu menu){
    	menu.questions=true;   	
    	menu.ia=false;
    	menu.obstacle=true;
    	menu.joueur=1;
    	menu.difficulte=1;
    	menu.fin_du_jeu=false;
    	menu.fin_du_menu=false;
    	menu.fin_du_snake=false;
    	return menu;
    }
    void keyTypedInConsole(char key) {
		switch(key) {
			case ANSI_UP:
			    deplacement = 'z';
			    break;
			case ANSI_DOWN:
			    deplacement = 's';
			    break;
			case ANSI_LEFT:
			    deplacement = 'q';
			    break;
			case ANSI_RIGHT:
			    deplacement = 'd';
			    break;
			case 'p':
			    valide = true;
			    break;
			
		}
    }
    class_menu afficher_menu(class_menu menu){
    	int place=0;
    	int save_place=42;
    	menu.fin_du_snake=false;
    	menu.fin_du_menu=false;
    	String [] mot=new String[]{"Jouer","Description","Paramètre","Quitter"};
    	String [] parametre=new String[]{"Questions","Snake","IA","Obstacle","Joueur","Difficulte","Revenir menu"};
    	while(!menu.fin_du_menu){
    		cursor(0, 0);
    		for (int i=0;i<length(mot) ;i++ ) {
    			if (i==place) {
    				println(ANSI_BLUE+i+". "+mot[i]+ANSI_WHITE );
    			}else{
    				println(i+". "+mot[i]);
    			}
    		}    		
    		save_place=place;
    		println("   ");
    		do{
    			place=readInt();
    		}while(!(place>=0 && place<=3));

    		
    		if (place==save_place) {
    			if(place==0){
    				menu.fin_du_menu=true;
    			}else if (place==1) {
    				print("\u001b[2J");
    				cursor(0, 0);
    				println("blabla");
    				do{
    					place=readInt();
    				}while(!(place==0));
    			}else if(place==2){
    				place=0;
    				do{
	    				print("\u001b[2J");
	    				cursor(0, 0);
	    				for (int i=0;i<length(parametre) ;i++ ) {
							if (i==place) {
								print(ANSI_BLUE+i+". "+parametre[i]+ANSI_WHITE );
							}else{
								print(i+". "+parametre[i]);
							}
							if (i==0) {
								if (menu.questions) {
									print(ANSI_GREEN + " activée");
								}else{
									print(ANSI_RED + " désactivée");
								}
							}else if (i==1) {
								if (menu.fin_du_snake) {
									print(ANSI_RED + " désactivée");
								}else{
									print(ANSI_GREEN + " activée");
								}	
							}else if (i==2) {
								if (menu.ia) {
									print(ANSI_GREEN + " activée");
							}else{
									print(ANSI_RED + " désactivée");
								}	
							}else if (i==3) {
								if (menu.obstacle) {
									print(ANSI_GREEN + " activée");
								}else{
									print(ANSI_RED + " désactivée");
								}	
							}else if (i==4) {
									print(ANSI_PURPLE +" "+ menu.joueur);
							}else if (i==5) {
									print(ANSI_PURPLE +" "+ menu.difficulte);
						}
							println(ANSI_WHITE);
	    				}    		
	    				save_place=place;
	    				do{
	    					place=readInt();
	    				}while(!(place>=0 && place<=6));
	    				if (place==save_place) {
	    					if(place==0){
	    						menu.questions=!(menu.questions);	
	    					}else if(place==1){
	    						menu.fin_du_snake=!(menu.fin_du_snake);
	    					}else if(place==2){
	    						menu.ia=!(menu.ia);
	    					}else if(place==3){
	    						menu.obstacle=!(menu.obstacle);
	    					}else if(place==4){
	    						menu.joueur=menu.joueur+1;
	    						if (menu.joueur==3) {
	    							menu.joueur=1;				
	    						}    							    						
	    					}else if(place==5){
	    						menu.difficulte=menu.difficulte+1;
	    						if (menu.difficulte==3) {
	    							menu.difficulte=1;					
	    						}
	    					}
	    				}
    				}while(!(place==6));
    				place=0;
    			}else if(place==3){
    				menu.questions=false;
    				menu.fin_du_snake=true;
    				menu.fin_du_jeu=true;
    				menu.fin_du_menu=true;
    				
    			}
    		}
    		
    		print("\u001b[2J");
    		cursor(0, 0);
    	}
    	return menu;
    }

    
    
    int pose_questions() {
		int compteur = 0;
		for (int i=1; i<11; i++) {
		    int chiffre1 = (int)(random()*11);
		    int chiffre2 = (int)(random()*11);
		    println("Question numéro " + i + " sur 10 :");
		    println("Combien font " + chiffre1 + " * " + chiffre2 + " ?");
		    int reponse = readInt();
		    if (reponse == (chiffre1*chiffre2)) {
			compteur = compteur + 1;
		    }
		}
		println("Vous avez " + compteur + " bonnes réponses sur 10.");
		return compteur;
    }
    
    int[][] initialisation(int hauteur, int largeur) {
		int[][] monde = new int[hauteur][largeur];
		for (int ligne=0; ligne<length(monde,1); ligne++) {
		    for (int colonne=0; colonne<length(monde,2); colonne++) {
			if (ligne==0 || colonne==0 || ligne==length(monde,1)-1 || colonne==length(monde,2)-1) {
			    monde[ligne][colonne]=1;
			} else {
			    monde[ligne][colonne]=0;
			}
		    }
		}
		return monde;
    }

    int[][] creerObstacles(int[][] mondePlein, int compteur) {
		double pourcentage=0.0;
		if (compteur == 1 || compteur == 2 || compteur == 0) {
		    pourcentage = 8.0;
		} else if (compteur == 3 || compteur == 4) {
		    pourcentage = 2.5;
		} else if (compteur == 5 || compteur == 6) {
		    pourcentage = 2.0;
		} else if (compteur == 7 || compteur == 8) {
		    pourcentage = 1.5;	    
		} else if (compteur == 9 || compteur == 10) {
		    pourcentage = 1.0;
		}
		for (int i=2; i<length(mondePlein,1)-2; i++) {
		    for (int j=2; j<length(mondePlein,2)-2; j++) {
			if (random()*11 < pourcentage && mondePlein[i-1][j-1] != 1
			                              && mondePlein[i-1][j+1] != 1
			                              && mondePlein[i+1][j-1] != 1
			                              && mondePlein[i+1][j+1] != 1) {
			    mondePlein[i][j] = 1;
			}
		    }
		}
		return mondePlein;
    }

   

    String afficher(int[][] monde, class_menu menu) {
		String affichage = "";
	        for (int i=0; i<length(monde,1); i++) {
			    if (i!=0) {
					affichage = affichage + "|"+"\n"+"\r";
			    }

		    for (int j=0; j<length(monde,2); j++) {
		    	if (menu.difficulte==1) {
					if (monde[i][j]==0) {
					    affichage = affichage + "|" + ".";
					} else {
					    affichage = affichage + "|" + monde[i][j];
					}
				}
				if (menu.difficulte==2) {
					if (i==0 || j==0 || i==length(monde,1)-1 || j==length(monde,2)-1) {
						affichage = affichage + "|" + monde[i][j];
					}else if(monde[i][j]==8){
					    affichage = affichage + "|" + monde[i][j];
					}else if(monde[i][j]>=2 && monde[i][j]<=4 ){
						affichage = affichage + "|" + monde[i][j];
					}else{

						if( monde[i][j-1]>=2 && monde[i][j-1]<=4 || 
							monde[i][j+1]>=2 && monde[i][j+1]<=4 || 
							monde[i-1][j]>=2 && monde[i-1][j]<=4 || 
							monde[i+1][j]>=2 && monde[i+1][j]<=4 ||
							monde[i-1][j-1]>=2 && monde[i-1][j-1]<=4 || 
							monde[i-1][j+1]>=2 && monde[i-1][j+1]<=4 || 
							monde[i+1][j+1]>=2 && monde[i+1][j+1]<=4 || 
							monde[i+1][j-1]>=2 && monde[i+1][j-1]<=4){
								if (monde[i][j]==0) {
									affichage = affichage + "|" + ".";	
								}else{
									affichage = affichage + "|" + "7";
								}
					 			
						}else if (i>1 && i<length(monde,1)-2 && j<length(monde,2)-2 && j>1) {
							
							if(monde[i][j-2]>=2 && monde[i][j-2]<=4 || 
								monde[i][j+2]>=2 && monde[i][j+2]<=4 || 
								monde[i-2][j]>=2 && monde[i-2][j]<=4 || 
								monde[i+2][j]>=2 && monde[i+2][j]<=4 ||
								monde[i-2][j-2]>=2 && monde[i-2][j-2]<=4 || 
								monde[i-2][j+2]>=2 && monde[i-2][j+2]<=4 || 
								monde[i+2][j+2]>=2 && monde[i+2][j+2]<=4 || 
								monde[i+2][j-2]>=2 && monde[i+2][j-2]<=4 ||

								monde[i-2][j+1]>=2 && monde[i-2][j+1]<=4 || 
								monde[i-1][j+2]>=2 && monde[i-1][j+2]<=4 || 
								monde[i+1][j+2]>=2 && monde[i+1][j+2]<=4 || 
								monde[i+2][j+1]>=2 && monde[i+2][j+1]<=4 ||
								monde[i+2][j-1]>=2 && monde[i+2][j-1]<=4 || 
								monde[i+1][j-2]>=2 && monde[i+1][j-2]<=4 || 
								monde[i-1][j-2]>=2 && monde[i-1][j-2]<=4 || 
								monde[i-2][j-1]>=2 && monde[i-2][j-1]<=4){
					 				if (monde[i][j]==0) {
									affichage = affichage + "|" + ".";	
								}else{
									affichage = affichage + "|" + "7";
								}

					 		}else{
								affichage = affichage + "|" + "|";
							}
						}else{
							affichage = affichage + "|" + "|";
						}
						
					}
		    		
		    	}
		    }
		}
		affichage = affichage + "|" + "\n" + "\r";
		return affichage;
    }

    int[][] creerSnake(int[][] monde, int taille) {
		int middleC = length(monde,2)/2;
		for (int i = 0 ; i < taille ; i++) {
		    if (i==0) {
			monde[1][middleC] = 4;
		    } else if (i==taille-1) {
			monde[1][middleC-i] = 2;
		    } else {
			monde[1][middleC-i] = 3;
		    }
		}
		return monde;
    }

	coordonne initialisation_mouvement(int [][]monde,coordonne coord,char [][]mouvement){
		for (int ligne=0;ligne<length(monde,1) ;ligne++ ) {
			for (int colone=0;colone<length(monde,2) ;colone++ ) {
				if(monde[ligne][colone]==4 || monde[ligne][colone]==3 || monde[ligne][colone]==2){	
					mouvement[ligne][colone]='d';
				}
				if (monde[ligne][colone]==4) {
				        coord.lT = ligne;
	   			        coord.cT = colone; 
				}else if(monde[ligne][colone]==2){
				        coord.lQ = ligne; 
					coord.cQ = colone; 
				}
			}		
		}
		return coord;
	}

	void suprimer_queue(int [][]monde,char [][]mouvement,coordonne coord){		
		monde[coord.lQ][coord.cQ]=0;		
		monde[coord.lT][coord.cT]=3;

	
	}

	coordonne changement_coord_et_mouvement(boolean pomme,char [][]mouvement,coordonne coord,int [][]monde){
		
		if (mouvement[coord.lT][coord.cT] == 'q') {
		        coord.cT = coord.cT-1;
		}
		if (mouvement[coord.lT][coord.cT]=='d') {
		        coord.cT = coord.cT+1;
		}
		if (mouvement[coord.lT][coord.cT]=='z') {
		        coord.lT=coord.lT-1;
		}
		if (mouvement[coord.lT][coord.cT]=='s') {
		        coord.lT=coord.lT+1;
		}
		if (pomme) {						
			if (mouvement[coord.lQ][coord.cQ]=='q') {
				mouvement[coord.lQ][coord.cQ]=' ';
			        coord.cQ=coord.cQ-1;		
			}else if (mouvement[coord.lQ][coord.cQ]=='d') {
				mouvement[coord.lQ][coord.cQ]=' ';
			        coord.cQ=coord.cQ+1;
			}else if (mouvement[coord.lQ][coord.cQ]=='z') {
				mouvement[coord.lQ][coord.cQ]=' ';
			        coord.lQ=coord.lQ-1;
			}else if (mouvement[coord.lQ][coord.cQ]=='s') {
				mouvement[coord.lQ][coord.cQ]=' ';
			        coord.lQ=coord.lQ+1;
			}
		}
		return coord;
	}

	class_menu rencontre_obstacle(class_menu menu, int [][]monde, coordonne coord){
		if(monde[coord.lT][coord.cT]==1 || monde[coord.lT][coord.cT]==3 || monde[coord.lT][coord.cT]==2){
			menu.fin_du_snake=true;
		}else{
			menu.fin_du_snake=false;
		}
		return menu;
	}
	boolean inplantation_pomme(boolean pomme,int [][]monde){
		if(!pomme){
			int x=0;
			int y=0;
			do{
				x=(int)(random()*length(monde,1));
				y=(int)(random()*length(monde,2));
			} while (monde[x][y]!=0);
			monde[x][y]=8;
			pomme=true;
		}
		return pomme;
	}

	boolean rencontre_pomme(boolean pomme, int [][]monde, coordonne coord){
		if (monde[coord.lT][coord.cT]==8) {			
			pomme=false;	
		}		
		return pomme;
	}
	void modification(int [][]monde,char [][]mouvement,coordonne coord){
		monde[coord.lT][coord.cT]=4;
		monde[coord.lQ][coord.cQ]=2;
		
	}
	
}
