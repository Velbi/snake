
public class Main {
	public static void main(String[] args) {
		Snake s=new Snake();
		
		int compteur = 10;//questions();
		int[][] monde = s.initialisation(25,30);
		char [][]mouvement=new char[monde.length][monde[0].length];
		boolean fin = false;
		int longeur=3;
		Point class_coord = new Point(0,0,0,0); 
	    monde = s.creerObstacles(monde,compteur);
		monde = s.creerSnake(monde,8);
	   	class_coord=s.initialisation_mouvement(monde,class_coord,mouvement); 
	   	boolean pomme=false;
	   	pomme=s.inplantation_pomme(pomme,monde,class_coord);
		while (!fin) {
		    System.out.println(s.afficher(monde));		    
		    s.suprimer_queue(monde,mouvement,class_coord);	    
		    s.changement_mouvement(mouvement,class_coord);
		    class_coord=changement_class_et_mouvement(pomme,mouvement,class_coord,monde);
		    fin=rencontre_obstacle(fin,monde,class_coord);
		    pomme=inplantation_pomme(pomme,monde,class_coord);
		    pomme=rencontre_pomme(pomme,monde,class_coord);
		    modification(monde,mouvement,longeur,class_coord);		    
		    monde=avancement_auto(monde,mouvement,longeur,class_coord);	    
		}
		System.out.println("fin du jeu");
	}
}
