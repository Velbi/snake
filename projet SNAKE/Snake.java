import java.util.Random;
import java.util.Scanner;

public class Snake {
	Random r=new Random();
	Scanner sc=new Scanner(System.in);

	
    int questions() {
		int compteur = 0;
		
		
		for (int i=1; i<11; i++) {
		    int chiffre1 = (int)(r.nextInt(11));
		    int chiffre2 = (int)(r.nextInt(11));
		    System.out.println("Question numéro " + i + " sur 10 :");
		    System.out.println("Combien font " + chiffre1 + " * " + chiffre2 + " ?");
		    int reponse = sc.nextInt();
		    if (reponse == (chiffre1*chiffre2)) {
			compteur = compteur + 1;
		    }
		}
		System.out.println("Vous avez " + compteur + " bonnes réponses sur 10.");
		return compteur;
    }

    int[][] initialisation(int hauteur, int largeur) {
		int[][] monde = new int[hauteur][largeur];
		for (int ligne=0; ligne<monde.length; ligne++) {
		    for (int colonne=0; colonne<monde[0].length; colonne++) {
			if (ligne==0 || colonne==0 || ligne==monde.length-1 || colonne==monde[0].length-1) {
			    monde[ligne][colonne]=1;
			} else {
			    monde[ligne][colonne]=0;
			}
		    }
		}
		return monde;
    }

    void testInitialisation() {
		int[][] testMonde = new int[][]{{0,0,0,0},
						{0,0,0,0},
						{0,0,0,0},
						{0,0,0,0}};
		//assertArrayEquals(testMonde, initialisation(4,4));
		
		int[][] testMonde2 = new int[][]{{0,0,0,0,0,0,0,0,0,0,0,0},
						 {0,0,0,0,0,0,0,0,0,0,0,0}};
		//assertArrayEquals(testMonde2, initialisation(2,12));
    }

    int[][] creerObstacles(int[][] mondePlein, int compteur) {
		double pourcentage=0.0;
		if (compteur == 1 || compteur == 2 || compteur == 0) {
		    pourcentage = 8.0;
		} else if (compteur == 3 || compteur == 4) {
		    pourcentage = 2.5;
		} else if (compteur == 5 || compteur == 6) {
		    pourcentage = 2.0;
		} else if (compteur == 7 || compteur == 8) {
		    pourcentage = 1.5;	    
		} else if (compteur == 9 || compteur == 10) {
		    pourcentage = 1.0;
		}
		for (int i=2; i<mondePlein.length-2; i++) {
		    for (int j=2; j<mondePlein[0].length-2; j++) {
			if (r.nextInt(11) < pourcentage && mondePlein[i-1][j-1] != 1
			                              && mondePlein[i-1][j+1] != 1
			                              && mondePlein[i+1][j-1] != 1
			                              && mondePlein[i+1][j+1] != 1) {
			    mondePlein[i][j] = 1;
			}
		    }
		}
		return mondePlein;
    }

    String afficher(int[][] monde) {
		String affichage = "";
	        for (int i=0; i<monde.length; i++) {
		    if (i!=0) {
			affichage = affichage + "|"+"\n";
		    }
		    for (int j=0; j<monde[0].length; j++) {
			if (monde[i][j]==0) {
			    affichage = affichage + "|" + ".";
			} else {
			    affichage = affichage + "|" + monde[i][j];
			}
		    }
		}
		affichage = affichage + "|";
		return affichage;
    }

    int[][] creerSnake(int[][] monde, int taille) {
		int middleC = monde[0].length/2;
		for (int i = 0 ; i < taille ; i++) {
		    if (i==0) {
			monde[1][middleC] = 4;
		    } else if (i==taille-1) {
			monde[1][middleC-i] = 2;
		    } else {
			monde[1][middleC-i] = 3;
		    }
		}
		return monde;
    }

	Point initialisation_mouvement(int [][]monde,Point class_coord,char [][]mouvement){
		for (int ligne=0;ligne<monde.length ;ligne++ ) {
			for (int colone=0;colone<monde[0].length ;colone++ ) {
				if(monde[ligne][colone]==4 || monde[ligne][colone]==3 || monde[ligne][colone]==2){	
					mouvement[ligne][colone]='d';
				}
				if (monde[ligne][colone]==4) {
					class_coord.setlT(ligne);;
	   				class_coord.setcT(colone); 
				}else if(monde[ligne][colone]==2){
					class_coord.setlQ(ligne); 
	    			class_coord.setcQ(colone);
				}
			}		
		}
		return class_coord;
	}

	void suprimer_queue(int [][]monde,char [][]mouvement,coordonne class_coord){		
		monde[class_coord.ligne_queue][class_coord.colonne_queue]=0;		
		monde[class_coord.ligne_tete][class_coord.colonne_tete]=3;

	
	}

	void changement_mouvement(char [][]mouvement,coordonne class_coord){
		//do{		
		
		println("Veuillez saisir un deplacement :");
		char deplacement = sc.next();
		//}while(equals(deplacement,'q') || equals(deplacement,'s') || equals(deplacement,'d') || equals(deplacement,'z'));
		mouvement[class_coord.ligne_tete][class_coord.colonne_tete]=deplacement;

	}

	coordonne changement_class_et_mouvement(boolean pomme,char [][]mouvement,coordonne class_coord,int [][]monde){
		
		if (mouvement[class_coord.ligne_tete][class_coord.colonne_tete]=='q') {
			class_coord.colonne_tete=class_coord.colonne_tete-1;
		}
		if (mouvement[class_coord.ligne_tete][class_coord.colonne_tete]=='d') {
			class_coord.colonne_tete=class_coord.colonne_tete+1;
		}
		if (mouvement[class_coord.ligne_tete][class_coord.colonne_tete]=='z') {
			class_coord.ligne_tete=class_coord.ligne_tete-1;
		}
		if (mouvement[class_coord.ligne_tete][class_coord.colonne_tete]=='s') {
			class_coord.ligne_tete=class_coord.ligne_tete+1;
		}
		if (pomme) {						
			if (mouvement[class_coord.ligne_queue][class_coord.colonne_queue]=='q') {
				mouvement[class_coord.ligne_queue][class_coord.colonne_queue]=' ';
				class_coord.colonne_queue=class_coord.colonne_queue-1;		
			}else if (mouvement[class_coord.ligne_queue][class_coord.colonne_queue]=='d') {
				mouvement[class_coord.ligne_queue][class_coord.colonne_queue]=' ';
				class_coord.colonne_queue=class_coord.colonne_queue+1;
			}else if (mouvement[class_coord.ligne_queue][class_coord.colonne_queue]=='z') {
				mouvement[class_coord.ligne_queue][class_coord.colonne_queue]=' ';
				class_coord.ligne_queue=class_coord.ligne_queue-1;
			}else if (mouvement[class_coord.ligne_queue][class_coord.colonne_queue]=='s') {
				mouvement[class_coord.ligne_queue][class_coord.colonne_queue]=' ';
				class_coord.ligne_queue=class_coord.ligne_queue+1;
			}
		}
		return class_coord;
	}

	boolean rencontre_obstacle(boolean fin, int [][]monde, coordonne class_coord){
		if(monde[class_coord.ligne_tete][class_coord.colonne_tete]==1 || monde[class_coord.ligne_tete][class_coord.colonne_tete]==3 || monde[class_coord.ligne_tete][class_coord.colonne_tete]==2){
			fin=true;
		}else{
			fin=false;
		}
		return fin;
	}
	boolean inplantation_pomme(boolean pomme,int [][]monde, coordonne class_coord){
		if(!pomme){
			int x=0;
			int y=0;
			do{
				x=(int)(random()*monde.lenght1));
				y=(int)(random()*monde.lenght2));
			}while(monde[x][y]!=0);
			monde[x][y]=8;
			pomme=true;
		}
		return pomme;
	}

	boolean rencontre_pomme(boolean pomme, int [][]monde, coordonne class_coord){
		if (monde[class_coord.ligne_tete][class_coord.colonne_tete]==8) {			
			pomme=false;	
		}		
		return pomme;
	}
	void modification(int [][]monde,char [][]mouvement,int longeur,coordonne class_coord){
		monde[class_coord.ligne_tete][class_coord.colonne_tete]=4;
		monde[class_coord.ligne_queue][class_coord.colonne_queue]=2;
		
	}
	int [][]avancement_auto(int [][]plateau,char [][]mouvement,int longeur,coordonne class_coord){
		return plateau;
	}
	

}
